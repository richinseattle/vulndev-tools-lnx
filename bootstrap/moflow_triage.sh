#!/bin/bash

PRINT () { 
	echo -ne "\n\n$1\n\n"
	sleep 0.5
}

usage () {
	echo "$0 -f <input to trace> -- <command to run>"
	exit 1
}

echo -e "Moflow slicer triage\n"


CRASH_INPUT=""

while getopts ":f:t:" options; do 
                                  
                                  
  case "${options}" in            
    f)                            
      CRASH_INPUT=${OPTARG}       
      ;;
    :)     
      echo "Error: -${OPTARG} requires an argument."
      usage
      ;;
    *)     
      usage
      ;;
  esac
done

shift $(($OPTIND - 1))
CMD="$@"

[ "$CRASH_INPUT" == "" ] && usage
[ "$CMD" == "" ] && usage


TARGET_BIN="$1"
TARGET_PATH=$(which $TARGET_BIN)
TARGET=$(basename $TARGET_BIN)
TRACE="$TARGET.trace"
IL="$TRACE.il"
SLICE="$IL.slice"
BITS=64
BITNESS_CHECK=`file "$TARGET_PATH" | grep "64-bit"`

[ "$BITNESS_CHECK" == "" ] && BITS=32



echo -e "[+] Tracing taint propagation from $CRASH_INPUT with pintool..\n"
../bap/pin/pin -t ../tracer/gentrace${BITS}.so -taint_indices -taint_files "$CRASH_INPUT" -o "$TRACE" -- $CMD 2>&1 | egrep 'Open|Changing|Taint|Thread'


PRINT "[+] Lifting trace and concretizing BAP IL.."
../utils/iltrans -trace "$TRACE" -trace-concrete-subst -trace-dsa -pp-ast "$IL" 2>&1 | grep -v WARNING


PRINT "[+] Tainted instructions executed in main image:"
[ "$BITS" == "32" ] && grep "addr 0x804" "$IL"
[ "$BITS" == "64" ] && grep "addr 0x4" "$IL"


PRINT "[+] Last 20 instructions executed before crash.."
grep "addr " "$IL" | tail -20


PRINT "[+] Display context for last executed instruction.."
tac "$IL" | grep  -m1 -B100 mov | tac | egrep 'addr |context' 

#TAINTED_CONTEXT=`tac "$IL" | grep -v "asm \"j" | grep  -m1 -B100 addr | tac | egrep 'addr |context'  | grep "\-1" | grep -v YMM | grep -v EFLAGS | cut -d "\"" -f 2 | head -1`
TAINTED_CONTEXT=`tac "$IL" | grep ", rd" | grep -v YMM | grep -v EFLAGS | grep -m1 ", -1, " | cut -d "\"" -f 2 | head -1`
#PRINT "[+] Finding DSA variable label for tainted $TAINTED_CONTEXT register.."
#SLICE_VAR=`tac "$IL" | grep -m2 $TAINTED_CONTEXT | tail -1 | cut -d ':' -f 3 | cut -d ' ' -f 3`
#SLICE_VAR=`tac "$IL" | grep dsa | grep -m1 $TAINTED_CONTEXT | grep -o "[^[:space:]]*$TAINTED_CONTEXT[^[:space:]]*" | sed 's/^[^-]*(//' | cut -d ':' -f 1 `
SLICE_VAR=`tac  "$IL" | grep -A100 "\"$TAINTED_CONTEXT\"" | grep dsa_$TAINTED_CONTEXT | grep -m1 "\=" | cut -d ':' -f 1`

PRINT "[+] Getting slice for tainted variable $SLICE_VAR.."
./slicer -il "$IL" -var $SLICE_VAR 2>&1 > "$SLICE"
egrep 'addr|context|symb' "$SLICE"

PRINT "[+] Getting context for first instruction tainted by $TAINTED_CONTEXT.."
grep -m1 addr "$SLICE"
grep -m1 -A100 addr "$SLICE" | grep context | grep mem | grep -v ", 0, rd"


TI="$SLICE.taint_intro"
grep -A1 "taint_intro" "$IL" > "$TI"

# Taint tags in original instruction leading to crash (subtract 1 for byte offset in first taint introduction)" 
PRINT "[*] Tainted offsets found!"
for i in `grep -m1 -A100 addr "$SLICE" | grep mem | grep -v ", 0, rd" | cut -d ' ' -f 7 | cut -d ',' -f 1` 
do
	BYTE_VALUE=`grep -m1 -A100 addr "$SLICE" | grep mem | grep -v ", 0, rd" | grep $i | cut -d ' ' -f 6 | cut -d ',' -f 1`
	L1=`grep -m1 "taint_intro $i" $TI`
	L2=`grep -A1 -m1 "taint_intro $i" $TI | tail -1`
	FNAME=`echo $L1 | cut -d '"' -f 2`
	OFFSET=`echo $L1| cut -d ' ' -f 5`
	ADDR=`echo $L2 | cut -d ' ' -f 2`
	ADDR=${ADDR//\"}
	V=`echo $L2 | cut -d ' ' -f 4`
	VAL=${V::-1}
	#echo "[*] Byte offset $(($i - 1)) with value $BYTE_VALUE in $CRASH_INPUT influenced the crash"
	echo "[*] Input source: $FNAME offset: $OFFSET mapped at $ADDR with value: $VAL"
done 
echo -n "\n"

#get all bytes read from original file mapping
#cat demo.trace.il | grep context | grep mem | grep -v ", 0, " | grep -v "u8, wr" | grep -v ", -1, "

