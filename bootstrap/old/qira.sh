#!/bin/bash

sudo mkdir -p /vulndev
sudo chown `whoami` /vulndev
cd /vulndev

# disable sudo timeouts
sudo sh -c 'echo "\nDefaults timestamp_timeout=-1">>/etc/sudoers'

# qira
(
git clone https://github.com/BinaryAnalysisPlatform/qira.git
  cd qira/
  ./install.sh
)

# enable sudo timeouts
sudo sed -i "/Defaults timestamp_timeout=-1/d" /etc/sudoers