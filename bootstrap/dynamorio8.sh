#!/bin/bash

cd /vulndev

mkdir -p dynamorio
curl -L "https://github.com/DynamoRIO/dynamorio/releases/download/release_8.0.0-1/DynamoRIO-Linux-8.0.0-1.tar.gz" | tar -zxf - -C dynamorio --strip 1

mkdir -p ~/bin
ln -s /vulndev/dynamorio/drmemory/bin64/drmemory ~/bin/
ln -s /vulndev/dynamorio/bin64/drrun  ~/bin/
ln -s /vulndev/dynamorio/tools/bin64/drcov2lcov ~/bin/
ln -s /vulndev/dynamorio/tools/bin64/genhtml ~/bin/

