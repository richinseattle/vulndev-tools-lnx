#!/bin/bash

sudo mkdir -p /vulndev
sudo chown `whoami` /vulndev
cd /vulndev


# Install 32 bit libs
sudo dpkg --add-architecture i386
sudo apt-get update
sudo apt-get -y install libc6:i386 libncurses5:i386 libstdc++6:i386
sudo apt-get -y install libc6-dev-i386


sudo apt-get -y install gdb valgrind python3-pip cmake
sudo pip3 install capstone unicorn keystone-engine ropper retdec-python
sudo apt-get -y install qemu-user-static qemu binfmt*

# pwndbg
git clone https://github.com/pwndbg/pwndbg
( cd pwndbg && ./setup.sh )

# Install radare2
git clone https://github.com/radare/radare2
( cd radare2 && /sys/install.sh )
sudo pip install r2pipe
#RR_PKG="/vagrant/rr-5.0.0-Linux-$(uname -m).deb"
#[ ! -f $RR_PKG ] && wget -O $RR_PKG https://github.com/mozilla/rr/releases/download/5.0.0/rr-5.0.0-Linux-$(uname -m).deb
#sudo dpkg -i $RR_PKG


# Install afl-utils
sudo pip2 install virtualenv virtualenvwrapper
sudo pip3 install virtualenv virtualenvwrapper
source /usr/local/bin/virtualenvwrapper.sh
git clone https://github.com/rc0r/afl-utils
cd afl-utils
mkvirtualenv afl -p /usr/bin/python3
python setup.py install
echo "source ~/.virtualenvs/afl/lib/python3.4/site-packages/exploitable-1.32_rcor-py3.4.egg/exploitable/exploitable.py" >> ~/.gdbinit


# Install crashwalk
# sudo apt-get -y install golang
# go get -u github.com/arizvisa/crashwalk/cmd/...



