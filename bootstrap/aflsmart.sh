#!/bin/bash

sudo apt-get install -y build-essential automake libtool libc6-dev-i386 python3-pip g++-multilib mono-mcs mono-devel

cd /vulndev

(
git clone https://github.com/aflsmart/aflsmart
cd aflsmart
make clean all
cd ..
) && \
(
cd aflsmart
wget https://sourceforge.net/projects/peachfuzz/files/Peach/3.0/peach-3.0.202-source.zip
unzip peach-3.0.202-source.zip
patch -p1 < peach-3.0.202.patch
cd peach-3.0.202-source
rm -rf ./Peach.Core.Analysis.Pin.*
CC=gcc CXX=g++ ./waf configure
# disable all pin related build stuf??
CC=gcc CXX=g++ ./waf install
)
