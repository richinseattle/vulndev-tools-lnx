#!/bin/bash

sudo mkdir -p /vulndev
sudo chown `whoami` /vulndev
cd /vulndev


# mcsema
(
sudo apt-get -y install build-essential git \
    dialog libstdc++6 python && \
    git clone https://github.com/trailofbits/mcsema.git && \
    cd mcsema && \
    ./bootstrap.sh
 )
