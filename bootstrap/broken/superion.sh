#!/bin/bash

cd /vulndev

sudo apt install -y libc++-dev clang-6.0

git clone https://github.com/zhunki/Superion
cd Superion 

export CC=clang-6.0
export CXX=clang++-6.0

cd tree_mutation
mkdir build
cd build
cmake ..
make
 
