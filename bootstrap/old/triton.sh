#!/bin/bash

sudo mkdir -p /vulndev
sudo chown `whoami` /vulndev
cd /vulndev

CPU_COUNT=$(getconf _NPROCESSORS_ONLN 2>/dev/null || getconf NPROCESSORS_ONLN 2>/dev/null || echo 1)

# disable sudo timeouts
sudo sh -c 'echo "\nDefaults timestamp_timeout=-1">>/etc/sudoers'

#triton 
(
sudo apt-get -y install git cmake build-essential clang ca-certificates curl \
    unzip libboost-dev python-dev python-pip 
# get and install the latest z3 relesae
cd /tmp && \
    curl -o z3.tgz -L  https://github.com/Z3Prover/z3/archive/z3-4.4.1.tar.gz && \
    tar zxf z3.tgz && cd z3-z3-4.4.1 && \
    CC=clang CXX=clang++ python scripts/mk_make.py && cd build && make -j$(( $CPU_COUNT -1 )) \
    && sudo make install && cd /tmp && rm -rf /tmp/z3-z3-4.4.1
)

# Install capstone
(
cd /tmp && \
    curl -o cap.tgz -L https://github.com/aquynh/capstone/archive/3.0.4.tar.gz && \
    tar xvf cap.tgz && cd capstone-3.0.4/ && sudo ./make.sh install && cd /tmp && \

    rm -rf /tmp/capstone-3.0.4
)

# now install Triton
# uncomment below to pull form git
# RUN cd /vulndev/pin-2.14-71313-gcc.4.4.7-linux/source/tools/ && git clone https://github.com/JonathanSalwan/Triton.git && \
#     cd Triton && mkdir build && cd build && cmake -G "Unix Makefiles" -DPINTOOL=on -DKERNEL4=on .. && \
#     make install && cd .. && python setup.py install
(
[ -z "$PIN_ROOT" ] && export PIN_ROOT="/vulndev/pin"
cd $PIN_ROOT/source/tools/ && \
   curl -o triton.zip -L https://github.com/JonathanSalwan/Triton/archive/master.zip && unzip triton.zip && cd Triton-master/ && mkdir build && cd build && \
   cmake -G "Unix Makefiles" -DPINTOOL=on -DKERNEL4=on .. && sudo make -j$(( $CPU_COUNT -1 )) install && cd ..
)

# enable sudo timeouts
sudo sed -i "/Defaults timestamp_timeout=-1/d" /etc/sudoers