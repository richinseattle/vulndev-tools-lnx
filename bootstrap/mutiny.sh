#!/bin/bash

( 
cd /vulndev
git clone https://github.com/Cisco-Talos/mutiny-fuzzer/ -b experiment
 
cd mutiny-fuzzer
cd radamsa && git submodule init && git submodule update
make
)
