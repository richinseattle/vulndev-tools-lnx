#!/bin/bash

cd /vulndev

sudo apt-get -y install \
        gcc-10 \
        g++-10 \
	cmake \
        ca-certificates \
        libelf-dev \
        libelf1 \
        libiberty-dev \
        libboost-all-dev \
        libtbb2 \
        libtbb-dev 

git clone https://github.com/dyninst/dyninst \
        && cd dyninst && mkdir build && cd build \
        && cmake .. \
        && sudo make \
        && sudo make install


