#!/bin/bash

cd /vulndev

git clone https://github.com/JonathanSalwan/Triton
cd Triton
mkdir build ; cd build
cmake ..
make -j3
sudo make install
