#!/bin/bash 

# American Fuzzy Lop w/ clang, qemu, dyninst, triforce support

# AFL standard binaries are installed to /usr/local/bin
# AFL qemu build is in /afl/qemu_mode
# afl-dyninst is installed to /usr/local/*
# Patched AFL binaries for TriforceAFL are in /TriforceAFL
# TriforceLinuxSyscallFuzzer is configured for fuzzing current kernel image

# afl-analyze  afl-clang-fast    afl-fuzz  afl-gotcpu   afl-tmin
# afl-clang    afl-clang-fast++  afl-g++   afl-plot     afl-whatsup
# afl-clang++  afl-cmin          afl-gcc   afl-showmap
# afl-dyninst  afl-qemu-trace    afl-qemu-system-trace

CPU_COUNT=$(getconf _NPROCESSORS_ONLN 2>/dev/null || getconf NPROCESSORS_ONLN 2>/dev/null || echo 1)

#sudo apt install -y libtool libtool-bin cmake libelf-dev libelf1 libiberty-dev libboost-all-dev automake bison libglib2.0-dev libpixman-1-dev python-setuptools clang-12
#sudo apt install -y build-essential python3-dev automake flex bison libglib2.0-dev libpixman-1-dev clang-12 python3-setuptools clang llvm-12 llvm-12-dev libstdc++-10-dev gcc-10-plugin-dev ninja-build
export LLVM_CONFIG=llvm-config-12
#export CC=gcc


#sudo apt-get update
sudo apt-get install -y build-essential python3-dev automake git flex bison libglib2.0-dev libpixman-1-dev python3-setuptools
# try to install llvm 12 and install the distro default if that fails
sudo apt-get install -y lld-12 llvm-12 llvm-12-dev clang-12 || sudo apt-get install -y lld llvm llvm-dev clang
sudo apt-get install -y gcc-$(gcc --version|head -n1|sed 's/.* //'|sed 's/\..*//')-plugin-dev libstdc++-$(gcc --version|head -n1|sed 's/.* //'|sed 's/\..*//')-dev

sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-10 10
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-10 10
sudo update-alternatives --install /usr/bin/clang++ clang++ /usr/bin/clang++-12 12
sudo update-alternatives --install /usr/bin/clang clang /usr/bin/clang-12 12
sudo update-alternatives --install /usr/bin/llvm-config llvm-config /usr/bin/llvm-config-12 12
sudo update-alternatives --install /usr/bin/llvm-symbolizer llvm-symbolizer /usr/bin/llvm-symbolizer-12 12



cd /vulndev

git clone https://github.com/AFLplusplus/AFLplusplus
git checkout tags/4.06c
ln -s AFLplusplus afl
cd AFLplusplus
make distrib $CPU_COUNT
make 
sudo make install
sudo sh -c 'echo core >/proc/sys/kernel/core_pattern'
