#!/bin/bash

cd /vulndev

git clone https://gitlab.com/rc0r/afl-utils.git
cd afl-utils
sudo python3 setup.py install 
echo "source /usr/local/lib/python3.8/dist-packages/exploitable-1.32_rcor-py3.8.egg/exploitable/exploitable.py" >> ~/.gdbinit
