#!/bin/bash

sudo mkdir -p /vulndev
sudo chown `whoami` /vulndev
cd /vulndev

#vuzzer
sudo apt-get -y update && \
    sudo apt-get -y install git build-essential wget python2.7 bmagic python-pip && \
    sudo pip install bitvector

( 
    cd /tmp
    git clone https://github.com/lemire/EWAHBoolArray.git && \
    cd EWAHBoolArray && sudo cp headers/* /usr/include 
) 

( git clone https://github.com/vusec/vuzzer.git && \
    cd vuzzer && ln -s ../../pin pin && \
    make support-libdft && make TARGET=amd64 && make -f mymakefile TARGET=amd64 
)
