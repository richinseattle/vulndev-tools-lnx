#!/bin/bash

sudo mkdir -p /vulndev
sudo chown `whoami` /vulndev
cd /vulndev

# Install pintool
mkdir -p pin-2.14
curl -L "http://software.intel.com/sites/landingpage/pintool/downloads/pin-2.14-71313-gcc.4.4.7-linux.tar.gz" | tar zxf - -C pin-2.14 --strip 1
rm -f /vulndev/pin
ln -s /vulndev/pin-2.14 /vulndev/pin

export PIN_ROOT="/vulndev/pin"
echo -e "\n\nexport PIN_ROOT=/vulndev/pin\n" >> ~/.bashrc

rm -f ~/bin/pin
ln -s /vulndev/pin/pin ~/bin/pin
