#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "Script must be run as root to bootstrap system"
   exit 1
fi

apt update -y 
apt install -y curl wget git unzip vim screen openssh-server sudo locate 
apt install -y build-essential libtool automake bison g++-multilib python3-pip python3-setuptools
apt install -y tcpdump tshark

apt install -y command-not-found
update-command-not-found

# disable sudo timeouts
echo -e "\nDefaults timestamp_timeout=-1">>/etc/sudoers

# add /usr/local/lib to LD_LIBRARY_PATH
echo /usr/local/lib >> /etc/ld.so.config
ldconfig

# create vulndev user 
useradd -G sudo -m -s /bin/bash -U vulndev 
echo vulndev:vulndev | chpasswd


# create vulndev working directory
mkdir -p /vulndev
chown vulndev:vulndev /vulndev

# create ~vulndev/bin
mkdir -p ~vulndev/bin
chown vulndev:vulndev ~vulndev/bin


# Ptrace enable
echo 0 | tee /proc/sys/kernel/yama/ptrace_scope

cat >> ~vulndev/.vimrc <<EOL
set ts=4
colorscheme desert
EOL
chown vulndev:vulndev ~vulndev/.vimrc

# re-enable sudo timeouts
# sed -i "/Defaults timestamp_timeout=-1/d" /etc/sudoers

cd ~vulndev
su -c "git clone https://richinseattle@bitbucket.org/richinseattle/vulndev-tools-lnx.git" vulndev
