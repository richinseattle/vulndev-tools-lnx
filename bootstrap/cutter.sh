CUTTER_VERSION="v2.0.2"
cd ~/bin
wget https://github.com/rizinorg/cutter/releases/download/${CUTTER_VERSION}/Cutter-${CUTTER_VERSION}-x64.Linux.AppImage
chmod +x Cutter-${CUTTER_VERSION}-x64.Linux.AppImage
ln -s $PWD/Cutter-${CUTTER_VERSION}-x64.Linux.AppImage cutter

