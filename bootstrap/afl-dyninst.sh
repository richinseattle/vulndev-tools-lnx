#!/bin/bash

cd /vulndev

git clone https://github.com/vanhauser-thc/afl-dyninst \
        && cd afl-dyninst \
        && ln -s ../AFLplusplus afl \
        && make \
        && sudo make install \
        && cd .. \
        && sudo sh -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/dyninst.conf && ldconfig' \
        && echo "export DYNINSTAPI_RT_LIB=/usr/local/lib/libdyninstAPI_RT.so" >> ~/.bashrc


