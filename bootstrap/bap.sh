#!/bin/bash

sudo mkdir -p /vulndev/bap
sudo chown `whoami` /vulndev/bap
cd /vulndev/bap

# BAP
sudo apt install -y libtinfo5


#sudo pip3 install bap networkx
wget https://github.com/BinaryAnalysisPlatform/bap/releases/download/v2.2.0/{bap,libbap,libbap-dev}_2.2.0.deb
sudo dpkg -i {bap,libbap,libbap-dev}_2.2.0.deb
rm *.deb

wget tiny.cc/bap-echo 

mkdir check_path
cd check_path
touch check_path.py
touch check_path.ml

