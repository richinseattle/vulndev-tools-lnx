#!/bin/bash

CPU_COUNT=$(getconf _NPROCESSORS_ONLN 2>/dev/null || getconf NPROCESSORS_ONLN 2>/dev/null || echo 1)

sudo apt-get -y install cmake3 ninja-build git curl doxygen gcc-multilib \
  					python-virtualenv python-dev thrift-compiler
sudo apt-get -y build-dep clang llvm 

cd /home/vagrant
mkdir -p llvm && cd llvm 

CLANG_SRC="/vagrant/llvm-project.tar.xz"
if [ -f "$CLANG_SRC" ]; then 
	echo "Decompressing LLVM source archive..."
	tar xf "$CLANG_SRC"
else
	echo "Checking out llvm project.."
	TOP_LEVEL_DIR=`pwd`
	git clone https://github.com/llvm-project/llvm-project-20170507/ llvm-project
	cd llvm-project
	git config branch.master.rebase true
	cd $TOP_LEVEL_DIR
	echo "Creating archive of src, this may take a while.."
	tar Jcf "$CLANG_SRC" llvm-project
fi

CLANG_BUILD="/vagrant/llvm-build.tar.xzXXX"
if [ -f "$CLANG_BUILD" ]; then 
	echo "Decompressing LLVM build archive..."
	tar xf "$CLANG_BUILD"
else
	mkdir llvm-build && cd llvm-build
	date > start_time
	cmake -G"Unix Makefiles" ../llvm-project/llvm -DLLVM_ENABLE_PROJECTS="clang;clang-tools-extra;klee" -DCMAKE_BUILD_TYPE=RelWithDebInfo -DLLVM_TARGETS_TO_BUILD=X86
	make -j$(( $CPU_COUNT -1 ))
	#cmake -GNinja ../llvm-project/llvm -DLLVM_ENABLE_PROJECTS="clang;clang-tools-extra;klee" -DCMAKE_BUILD_TYPE=RelWithDebInfo -DLLVM_TARGETS_TO_BUILD=X86
	#ninja 
	echo "start time: `cat start_time`"
	echo "  end time: `date`"
	rm -f start_time
fi




#cat >>/home/vagrant/.profile <<EOL


# Path of `scan-build.py` (intercept-build)
# NOTE: SKIP this line if you don't want to use intercept-build.
#export PATH=/home/vagrant/clang/tools/scan-build-py/bin:$PATH

# Path of the built LLVM/Clang
# NOTE: SKIP this line if clang is available in your PATH as an installed Linux package.
#export PATH=~/<user path>/build/bin:$PATH
#EOL