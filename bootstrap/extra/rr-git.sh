cd /vulndev

sudo apt-get install ccache cmake make g++-multilib gdb \
  pkg-config coreutils python3-pexpect manpages-dev git \
  ninja-build capnproto libcapnp-dev

git clone https://github.com/mozilla/rr.git
mkdir build && cd build
cmake ../rr

make -j
sudo make install

echo 'kernel.perf_event_paranoid=1' | sudo tee '/etc/sysctl.d/51-enable-perf-events.conf'

# for vmware add this to vmx
# monitor_control.disable_hvsim_clusters = true

