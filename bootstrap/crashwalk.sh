#!/bin/bash

cd /vulndev

sudo apt-get -y install golang

echo "export CW_EXPLOITABLE=/vulndev/exploitable/exploitable/exploitable.py" >> ~/.bashrc

mkdir crashwalk
cd crashwalk
export GOPATH=`pwd`
go get -u github.com/bnagy/crashwalk/cmd/...
cd bin
for i in `pwd`/* ; do sudo ln -s $i /usr/local/bin ; done
