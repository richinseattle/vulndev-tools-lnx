#!/bin/bash 

# American Fuzzy Lop w/ clang, qemu, dyninst, triforce support

# AFL standard binaries are installed to /usr/local/bin
# AFL qemu build is in /afl/qemu_mode
# afl-dyninst is installed to /usr/local/*
# Patched AFL binaries for TriforceAFL are in /TriforceAFL
# TriforceLinuxSyscallFuzzer is configured for fuzzing current kernel image

# afl-analyze  afl-clang-fast    afl-fuzz  afl-gotcpu   afl-tmin
# afl-clang    afl-clang-fast++  afl-g++   afl-plot     afl-whatsup
# afl-clang++  afl-cmin          afl-gcc   afl-showmap
# afl-dyninst  afl-qemu-trace    afl-qemu-system-trace

CPU_COUNT=$(getconf _NPROCESSORS_ONLN 2>/dev/null || getconf NPROCESSORS_ONLN 2>/dev/null || echo 1)

# disable sudo timeouts
sudo sh -c 'echo "\nDefaults timestamp_timeout=-1">>/etc/sudoers'


sudo mkdir -p /vulndev
sudo chown -R `whoami` /vulndev
cd /vulndev

# install afl w/ llvm + qemu
sudo apt-get -y install build-essential curl clang git libtool wget \
  cmake libelf-dev libelf1 libiberty-dev libboost-all-dev automake bison llvm llvm-dev

[ -f /usr/bin/llvm-config-3.8 ] && sudo ln -s /usr/bin/llvm-config-3.8 /usr/bin/llvm-config
[ -f /usr/bin/llvm-config-3.4 ] && sudo ln -s /usr/bin/llvm-config-3.4 /usr/bin/llvm-config

sudo apt-get -y build-dep qemu

AFL_DIR=$PWD/afl
rm -rf $AFL_DIR
git clone https://github.com/mirrorer/afl && \
  ( cd $AFL_DIR && make -j$(( $CPU_COUNT -1 )) ) && \
  ( cd $AFL_DIR/llvm_mode && make -j$(( $CPU_COUNT -1 )) ) && \
  ( cd $AFL_DIR/qemu_mode && ./build_qemu_support.sh ) && \
  ( cd $AFL_DIR && sudo make install )



sudo apt-get -y autoremove

# enable sudo timeouts
sudo sed -i "/Defaults timestamp_timeout=-1/d" /etc/sudoers
