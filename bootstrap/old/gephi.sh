# Gephi
(
if [ ! -f /usr/bin/gephi ] ; then 
  sudo apt-get -y install default-jre
  sudo mkdir -p /opt/gephi
  sudo chown `whoami` /opt/gephi
  cd /opt/gephi
  wget https://github.com/gephi/gephi/releases/download/v0.9.2/gephi-0.9.2-linux.tar.gz
  tar xf gephi-0.9.2-linux.tar.gz
  rm gephi-0.9.2-linux.tar.gz
  sudo ln -s /opt/gephi/gephi-0.9.2/bin/gephi /usr/bin
fi
)
