#!/bin/bash

cd /vulndev

# Install pintool
PINVER="3.17"
mkdir -p "pin-$PINVER"
curl -L "https://software.intel.com/sites/landingpage/pintool/downloads/pin-3.17-98314-g0c048d619-gcc-linux.tar.gz" | tar zxf - -C "pin-$PINVER" --strip 1
rm -f /vulndev/pin
ln -s /vulndev/pin-$PINVER /vulndev/pin

export PIN_ROOT="/vulndev/pin"
echo -e "\n\nexport PIN_ROOT=/vulndev/pin\n" >> ~/.bashrc

rm -f ~/bin/pin
ln -s /vulndev/pin/pin ~/bin/pin
