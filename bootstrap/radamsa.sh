#!/bin/bash

cd /vulndev
git clone https://gitlab.com/akihe/radamsa.git
cd radamsa
make

mkdir -p ~/bin
ln -s /vulndev/radamsa/bin/radamsa ~/bin
