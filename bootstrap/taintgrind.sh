VALGRIND_VERSION="3.18.1"

cd /tmp
wget https://sourceware.org/pub/valgrind/valgrind-${VALGRIND_VERSION}.tar.bz2 
tar -jxf valgrind-${VALGRIND_VERSION}.tar.bz2 -C /vulndev
rm valgrind-${VALGRIND_VERSION}.tar.bz2
ln -s /vulndev/valgrind-${VALGRIND_VERSION} /vulndev/valgrind
cd /vulndev/valgrind
#./autogen.sh && \
#    ./configure --prefix=`pwd`/build && \
#    make install -i

git clone http://github.com/wmkhoo/taintgrind
cd taintgrind
./build_taintgrind.sh

cd /vulndev
git clone https://github.com/marekzmyslowski/rtaint
cd rtaint
sudo python3 setup.py install

cd examples
make
taintgrind --file-filter=`pwd`/crash.input --taint-start=0 --taint-len=25 --compact=yes ./avBranch crash.input 2>log.txt
rtaint -f ./examples/log.txt



