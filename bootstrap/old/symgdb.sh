#!/bin/bash

sudo mkdir -p /vulndev
sudo chown `whoami` /vulndev
cd /vulndev

sudo cp -f /usr/bin/gdb /usr/bin/gdb.orig
git clone https://github.com/SQLab/symgdb
cd symgdb/
./install.sh 
echo "source /vulndev/symgdb/symgdb.py" >> ~/.gdbinit


