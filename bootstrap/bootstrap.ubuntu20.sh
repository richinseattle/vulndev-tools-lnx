#!/bin/bash

#if [[ $EUID -eq 0 ]]; then
#   echo "Do not run as root, this script will sudo as needed" 
#   exit 1
#fi

# disable sudo timeouts
sudo sh -c 'echo "\nDefaults timestamp_timeout=-1">>/etc/sudoers'

# add /usr/local/lib to LD_LIBRARY_PATH
#sudo bash -c 'echo /usr/local/lib >> /etc/ld.so.config'
#sudo ldconfig

# create ~/bin
mkdir -p ~/bin
cp ../bin/* ~/bin
echo "PATH=$PATH:$HOME/bin" >> $HOME/.bashrc

# create vulndev
#sudo mkdir -p /vulndev
#sudo chown $USER:$USER /vulndev
#cd /vulndev

sudo apt-get -y install curl wget git unzip neovim screen openssh-server
sudo apt-get -y install build-essential libtool automake bison g++-multilib \
  python3-pip python3-setuptools clang-11
sudo apt-get -y install graphviz ranger tcpdump tshark

sudo apt install -y command-not-found
sudo update-command-not-found

sudo apt install -y gcc-10 g++-10 clang-11 llvm-11 libc6-dev make dpkg-dev automake autoconf bison libtool libtool-bin
sudo apt install -y clang++-11
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

#sudo apt-get install mono-complete
sudo apt install -y python python3 python3-pip

sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-10 10
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-10 10
sudo update-alternatives --install /usr/bin/clang++ clang++ /usr/bin/clang++-11 11
sudo update-alternatives --install /usr/bin/clang clang /usr/bin/clang-11 11
sudo update-alternatives --install /usr/bin/llvm-config llvm-config /usr/bin/llvm-config-11 11

# Ptrace enable
sudo sh -c 'echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope'

cat >>~/.vimrc <<EOL
set ts=4
colorscheme industry
EOL

echo -e "\n\nexport CPU_COUNT=\$(getconf _NPROCESSORS_ONLN 2>/dev/null || getconf NPROCESSORS_ONLN 2>/dev/null || echo 1)\n" >> ~/.bashrc

#source dynamorio.sh
#source pin.sh
#source dyninst.sh
#source frida.sh
#source clang.sh
#source ramdisk.sh

# enable sudo timeouts
#sudo sed -i "/Defaults timestamp_timeout=-1/d" /etc/sudoers
