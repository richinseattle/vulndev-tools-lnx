cd /vulndev

sudo apt-get install -y \
        cargo \
        clang-10 \
        cmake \
        g++ \
        git \
        libz3-dev \
        llvm-10-dev \
        llvm-10-tools \
        ninja-build \
        python2 \
        python3-pip \
        zlib1g-dev 

sudo pip3 install lit

sudo apt install libz3-dev -y


git clone https://github.com/eurecom-s3/symcc.git
cd symcc
git submodule update --init
mkdir build
cd build
cmake -G Ninja -DQSYM_BACKEND=ON -DZ3_TRUST_SYSTEM_VERSION=on ..
ninja
cd ..

sudo apt build-dep qemu
git clone https://github.com/eurecom-s3/symqemu
cd symqemu
./configure                                                    \
      --audio-drv-list=                                           \
      --disable-bluez                                             \
      --disable-sdl                                               \
      --disable-gtk                                               \
      --disable-vte                                               \
      --disable-opengl                                            \
      --disable-virglrenderer                                     \
      --disable-werror                                            \
      --target-list=x86_64-linux-user                             \
      --enable-capstone=git                                       \
      --symcc-source=`pwd`/../                       \
      --symcc-build=`pwd`/../build

make -j$CPU_COUNT
