#!/bin/bash

cd /vulndev

# set workdir path
export WORKDIR="$(pwd)/nautilus"
export LLVM_CONFIG=llvm-config-11
export CC=clang-11
export CXX=clang++-11

git clone https://github.com/nautilus-fuzz/nautilus
cd nautilus
rustup override set nightly
afl-clang-fast test.c -o test 
cargo build --release
#mkdir /tmp/workdir
#cargo run --release -- -g grammars/grammar_py_example.py -o /tmp/workdir -- ./test @@

