#!/bin/sh

# Visual Studio Code 
( 
#sudo apt install -y libxss1 # debian8
mkdir vscode && cd vscode && \
wget -O vscode.deb "https://go.microsoft.com/fwlink/?LinkID=760868" && \
sudo dpkg -i *.deb && \
sudo apt-get -y install -f && \
cd .. && rm -rf vscode && \
echo -e "\n\nalias vscode='code'\n" >> ~/.bashrc && \
echo -e "\nVisual Studio Code installed successfully\n"
)
