cd /vulndev
git clone https://github.com/vusec/vuzzer64
export PIN_HOME=/vulndev/pin
export PIN_ROOT=/vulndev/pin
export DFT_HOME=/vulndev/angora/libdft64
cd vuzzer64
cd fuzzer-code

# bbcounts2.cpp may need these suppressions at lines 165 & 167
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

make
cd ../libdft64
make
