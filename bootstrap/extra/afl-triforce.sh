#!/bin/bash

cd /vulndev
mkdir afl-triforce
cd afl-triforce

CPU_COUNT=$(getconf _NPROCESSORS_ONLN 2>/dev/null || getconf NPROCESSORS_ONLN 2>/dev/null || echo 1)

sudo apt-get -y build-dep qemu 
sudo apt-get -y install build-essential curl git \
      libtool-bin wget automake bison zlib1g zlib1g-dev \
      linux-image-$(uname -r)

# install TriforceAFL
export TRIFORCE_KERNEL=kern
git clone https://github.com/nccgroup/TriforceAFL && \
  ( cd TriforceAFL && make -j$(( $CPU_COUNT -1 )) ) && \
  git clone https://github.com/nccgroup/TriforceLinuxSyscallFuzzer && \
  ( cd TriforceLinuxSyscallFuzzer && \
    make -j$(( $CPU_COUNT -1 )) && \
    mkdir $TRIFORCE_KERNEL && \
    sudo cp /boot/vmlinuz* $TRIFORCE_KERNEL/bzImage && \
    sudo cp /boot/System.map* $TRIFORCE_KERNEL/kallsyms && \
    make inputs )

