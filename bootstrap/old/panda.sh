#!/bin/bash

sudo mkdir -p /vulndev
sudo chown `whoami` /vulndev
cd /vulndev

# disable sudo timeouts
sudo sh -c 'echo "\nDefaults timestamp_timeout=-1">>/etc/sudoers'

# panda

sudo add-apt-repository -y ppa:phulin/panda
sudo apt-get update
sudo apt-get -y build-dep qemu
sudo apt-get -y install python-pip git protobuf-compiler protobuf-c-compiler \
  libprotobuf-c0-dev libprotoc-dev libelf-dev \
  libcapstone-dev libdwarf-dev python-pycparser llvm-3.3 llvm-3.3-dev clang-3.3 libc++-dev

git clone https://github.com/panda-re/panda panda-qemu

mkdir -p panda-qemu/build
cd panda-qemu/build
../build.sh

sudo sed -i "/Defaults timestamp_timeout=-1/d" /etc/sudoers

