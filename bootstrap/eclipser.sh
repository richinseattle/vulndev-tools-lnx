#!/bin/sh

cd /vulndev

sudo apt-get build-dep -y qemu
sudo apt-get install -y libtool libtool-bin wget automake autoconf bison gdb
sudo apt install -y libgtk-3-dev libvte-2.91-dev libepoxy-dev libegl-mesa0
sudo apt-get install -y apt-transport-https

wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
rm packages-microsoft-prod.deb
sudo apt-get update
sudo apt-get install -y dotnet-runtime-5.0 dotnet-sdk-5.0 dotnet-runtime-2.1

git clone https://github.com/SoftSec-KAIST/Eclipser eclipser
cd eclipser
CC=gcc CXX=g++ CFLAGS="-w --std=c11" make
