#!/bin/bash

echo "Setting up system environment... "
sudo echo

echo "     disabling system crash notifiers..."
sudo bash -c 'echo core > /proc/sys/kernel/core_pattern'

echo "     disabling cpu scaling..."
sudo bash <<"EOF"
for i in $(seq `nproc`); do
     let "cpu_idx = $i - 1";
     echo performance > /sys/devices/system/cpu/cpu$cpu_idx/cpufreq/scaling_governor;
done
EOF

echo "     disabling address space randomization..."
sudo bash -c "echo 0 > /proc/sys/kernel/randomize_va_space"

echo "     disabling swap disks... (this may take some time)"
sudo bash -c "swapoff -a"

sudo mkdir -p /fuzzing/ramdisk 
sudo chown `whoami` /fuzzing/ramdisk
echo "     creating 2GB tmpfs at /fuzzing/ramdisk..."
if [ "`mount | grep /fuzzing/ramdisk`1" == "1" ] 
then 
     sudo bash -c 'mkdir -p /fuzzing/ramdisk'
     sudo mount -t tmpfs -o size=2G tmpfs /fuzzing/ramdisk
else 
     echo "          ramdisk already mounted at /fuzzing/ramdisk, skipping..."; 
fi

echo "     copying files..."
echo

echo "Launching fuzzer"
#./afl-fuzz -i /fuzzing/ramdisk/input -o /fuzzing/ramdisk/output ./libpngfuzz @@


